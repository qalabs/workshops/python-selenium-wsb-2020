# Test method 1 that simply passes
def test_1():
    pass


# Test method 2 that fails with exception
def test_2():
    raise NotImplementedError("Not implemented")


# Assert two strings are equal
def test_3():
    assert "abc" is "abc"


# Assert two strings are equal (fails)
def test_4():
    assert "abc" is not "abc"


# Assert two numbers are equal (fails with message)
def test_5():
    assert "abc" is not "abc", "'abc' and 'abc' are equal"
