import pytest
from _2_pytest.samples.custom_list import CustomList


@pytest.fixture
def user_list():
    # create and return object under test
    return CustomList([1, 2, 3, 4, 5])


def test_clear(user_list):
    user_list.clear()
    assert user_list.len() == 0


def test_len(user_list):
    assert user_list.len() == 5
