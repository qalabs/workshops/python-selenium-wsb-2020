"""
Global configuration for all samples and exercises
"""

import os

# Project settings

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__)) + os.sep + ".." + os.sep
SCREENSHOTS_DIR = PROJECT_ROOT + "screenshots"
TMP_DIR = PROJECT_ROOT + "tmp"

# Append to PATH (if you want your local drivers to be used in the project)
os.environ["PATH"] += os.pathsep + PROJECT_ROOT + "selenium-drivers"

# Applications URLs

WEB_SAMPLES_URL = "https://qalabs.pl/demos/web-samples"
CONTACTS_APP_URL = "https://qalabs.gitlab.io/vuejs-contacts-demo"
