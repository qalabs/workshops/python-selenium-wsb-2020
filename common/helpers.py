import os

from selenium.webdriver.chrome.options import Options as ChromeOptions

from common import config  # import global properties
from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager


def new_driver(driver="firefox", implicitly_wait=1):
    if driver == "firefox":
        profile = webdriver.FirefoxProfile()
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.manager.showWhenStarting', False)
        profile.set_preference('browser.download.dir', os.path.abspath(config.TMP_DIR))
        profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'text/csv, text/txt, application/vnd.ms-excel')
        profile.set_preference('general.warnOnAboutConfig', False)
        driver = webdriver.Firefox(executable_path=GeckoDriverManager().install(), firefox_profile=profile)
    if driver == "chrome":
        # driver = webdriver.Chrome()
        options = ChromeOptions()
        # the path must be absolute, otherwise Chrome won't download the file
        options.add_experimental_option("prefs", {"download.default_directory": os.path.abspath(config.TMP_DIR)})
        driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=options)
    driver.implicitly_wait(implicitly_wait)
    driver.maximize_window()
    return driver


def click(driver, css_selector):
    elements = driver.find_elements_by_css_selector(css_selector)
    if len(elements) > 0 and not elements[0].is_disabled():
        elements[0].click()


def is_displayed(driver, css_selector):
    elements = driver.find_elements_by_css_selector(css_selector)
    return len(elements) > 0 and elements[0].is_displayed()


def is_disabled(driver, css_selector):
    elements = driver.find_elements_by_css_selector(css_selector)
    return len(elements) > 0 and elements[0].is_disabled()


def wait_until_displayed(driver, css_selector):
    from selenium.webdriver.support.wait import WebDriverWait
    WebDriverWait(driver=driver, timeout=5) \
        .until(lambda driver: driver.find_element_by_css_selector(css_selector).is_displayed())


def wait_until_disappeared(driver, css_selector):
    from selenium.webdriver.support.wait import WebDriverWait
    WebDriverWait(driver=driver, timeout=5) \
        .until_not(lambda _: driver.find_element_by_css_selector(css_selector).is_displayed())
