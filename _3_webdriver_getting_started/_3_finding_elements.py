import pytest
from selenium.webdriver import Remote
from common import config, helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample2.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_finds_button_by_title(driver: Remote):
    # TODO Find disabled button (disabled=disabled, type=button) (find_element_by_css_selector)
    button = None
    # TODO Get data-custom-attribute value (get_attribute)
    attr_value = None

    assert not button.is_enabled()
    assert button.text == "DISABLED"
    assert "My custom attribute!" == attr_value

    # TODO assert button is displayed
    assert button.is_displayed()


def test_verifies_3_subheadings_on_the_page(driver: Remote):
    # TODO Find all h with class=md-subheading inside div with id=paragraphs (find_elements_by_css_selector)
    divs = []

    assert len(divs) == 3


def test_verifies_ordered_list_items(driver: Remote):
    # TODO Find all list items in ol with id=ordered-list (find_element_by_css_selector)
    list_items = None
    # TODO In list_items_texts array store all list item texts
    list_items_texts = []

    assert "List Item 1" in list_items_texts
    assert "List Item 2" in list_items_texts
    assert "List Item 3" in list_items_texts


def test_verifies_links_texts(driver: Remote):
    # TODO Find div with id=links (find_element_by_id)
    div = None
    # TODO Inside the div object find all links (find_elements_by_tag_name)
    links = None
    # TODO In link_texts array store all link texts
    link_texts = []

    assert "Here we go!" in link_texts
    assert "Link 1 (clicked: false)" in link_texts
    assert "Link 2 (clicked: false)" in link_texts
    assert "Link 3 (clicked: false)" in link_texts


def test_verifies_names_in_the_table(driver: Remote):
    # TODO Find all cells with names (find_elements_by_css_selector)
    name_cells = None
    # TODO In names array store all names
    names = []

    assert "Shawna Dubbin" in names
    assert "Odette Demageard" in names
    assert "Vera Taleworth" in names
