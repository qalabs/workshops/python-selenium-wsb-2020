import pytest
from selenium.webdriver import Remote
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

from common import config, helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample2.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_finds_element_by_id(driver: Remote):
    element = driver.find_element_by_id("paragraphs")


def test_finds_element_by_css_selector(driver: Remote):
    element = driver.find_element_by_css_selector("#app div.md-card-header > div.md-title")


def test_finds_element_by_class_name(driver: Remote):
    element = driver.find_element_by_class_name("md-title")


def test_finds_element_by_tag_name(driver: Remote):
    element = driver.find_element_by_tag_name("p")


def test_finds_element_by_link_text(driver: Remote):
    element = driver.find_element_by_link_text("Here we go!")


def test_finds_element_by_partial_link_text(driver: Remote):
    element = driver.find_element_by_partial_link_text("we")


def test_finds_element_by_xpath(driver: Remote):
    element = driver.find_element_by_xpath('//*[@id="app"]')


def test_find_elements_by_css_selector(driver: Remote):
    elements = driver.find_elements_by_css_selector("#app div")


def test_finds_elements_by_class_name(driver: Remote):
    elements = driver.find_elements_by_class_name("md-title")


def test_finds_elements_by_tag_name(driver: Remote):
    elements = driver.find_elements_by_tag_name("p")


def test_finds_elements_by_link_text(driver: Remote):
    elements = driver.find_elements_by_link_text("Link 1")


def test_finds_elements_by_partial_link_text(driver: Remote):
    elements = driver.find_elements_by_partial_link_text("Link")


def test_finds_elements_by_xpath(driver: Remote):
    elements = driver.find_elements_by_xpath('//*[@id="app"]//p')


def test_generic_find(driver: Remote):
    element = driver.find_element(By.CSS_SELECTOR, "div")
    elements = driver.find_elements(By.CSS_SELECTOR, "div")


def test_no_such_element_exception(driver: Remote):
    try:
        driver.find_element_by_id("non_existing_id")
    except NoSuchElementException:
        print("Not found!")


def test_no_elements_found(driver: Remote):
    web_elements_empty = driver.find_elements_by_class_name("non_existing_class")
    assert len(web_elements_empty) == 0
