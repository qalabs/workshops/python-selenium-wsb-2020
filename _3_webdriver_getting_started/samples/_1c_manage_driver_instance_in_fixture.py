import pytest

from common import config

from selenium.webdriver import *
from webdriver_manager.firefox import GeckoDriverManager


@pytest.fixture
def driver():
    driver = Firefox(executable_path=GeckoDriverManager().install())
    driver.get(config.WEB_SAMPLES_URL + "/sample1.html")
    yield driver
    driver.close()


def test_1(driver: Remote):
    assert "Sample 1" == driver.title


def test_2(driver: Remote):  # Assign type
    assert "Sample 1" == driver.title
