import os

from common import config

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager


def test_firefox():
    profile = webdriver.FirefoxProfile()
    profile.set_preference('browser.download.folderList', 2)
    profile.set_preference('browser.download.manager.showWhenStarting', False)
    profile.set_preference('browser.download.dir', os.getcwd())
    profile.set_preference('browser.helperApps.neverAsk.saveToDisk', '*/*')
    profile.set_preference('general.warnOnAboutConfig', False)

    driver = webdriver.Firefox(
        executable_path=GeckoDriverManager().install(),
        firefox_profile=profile
    )

    # Wait until page is loaded
    driver.get(config.WEB_SAMPLES_URL + "/sample1.html")

    # Assert page title
    assert "Sample 1" == driver.title

    # Close the driver
    driver.close()


def test_chrome():
    chrome_options = Options()
    chrome_options.accept_insecure_certs = True
    # chrome_options.add_argument(f"--user-data-dir={config.TMP_DIR}/chrome-data-dir")

    driver = webdriver.Chrome(
        executable_path=ChromeDriverManager().install(),
        chrome_options=chrome_options)

    # Implicit wait for element to be found
    driver.implicitly_wait(1)

    # Wait until page is loaded
    driver.get(config.WEB_SAMPLES_URL + "/sample1.html")

    # Assert page title
    assert "Sample 1" == driver.title

    # Close the driver
    driver.close()
