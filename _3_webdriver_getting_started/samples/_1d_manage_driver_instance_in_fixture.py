import pytest
from common import config, helpers

from selenium.webdriver import Remote


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample1.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_1(driver: Remote):
    assert "Sample 1" == driver.title


def test_2(driver: Remote):
    assert "Sample 1" == driver.title


def test_3(driver: Remote):
    assert "Sample 1" == driver.title
