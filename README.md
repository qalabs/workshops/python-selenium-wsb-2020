Test Automation with Python, Pytest and Selenium
================================================

## Prerequisites

- Git, Terminal (e.g. [cmder](https://blog.qalabs.pl/narzedzia/git-cmder/))
- Python 3.8+
- [PyCharm](https://blog.qalabs.pl/narzedzia/python-pycharm)
- Up to date Chrome and Firefox browsers

## See

- Check `common` package to see configuration options

## Windows

- Open terminal
- Prepare virtual environment by running:


	venv-install.bat


- Install dependencies:


    pip install -r requirements.txt


- Import the project to PyCharm

### How to activate the virtual environment after you restart the shell?

    venv/Scripts/activate.bat

## macOS

- Open terminal
- Prepare virtual environment


	python -m venv venv
	source venv/bin/activate
	

- Install dependencies:


	pip install -r requirements.txt


- Run all tests:


    pytest tests

    
- Import project to PyCharm