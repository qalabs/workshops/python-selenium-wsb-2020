from time import sleep

import pytest
from selenium.webdriver import Remote
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

from common import config, helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver(implicitly_wait=0.25)
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample4.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_wait_for_snackbar(driver: Remote):
    submit = driver.find_element_by_id("submit")
    submit.click()

    wait = WebDriverWait(driver, 5)
    wait.until(ec.visibility_of_element_located((By.ID, "snackbar")))

    snackbar_text = driver.find_element_by_css_selector(".md-snackbar-content > span").text
    assert "Form submitted!" == snackbar_text


def test_wait_for_snackbar_text(driver: Remote):
    submit = driver.find_element_by_id("submit")
    submit.click()

    wait = WebDriverWait(driver, 5)
    snackbar_text_present = wait.until(
        ec.text_to_be_present_in_element((By.CSS_SELECTOR, ".md-snackbar-content > span"), "Form submitted!"))

    assert snackbar_text_present


def test_wait_for_snackbar_text_with_lambda(driver: Remote):
    submit = driver.find_element_by_id("submit")
    submit.click()

    wait = WebDriverWait(driver, 5)
    snackbar_text = wait.until(lambda _: driver.find_element_by_css_selector(".md-snackbar-content > span").text)

    assert "Form submitted!" == snackbar_text
