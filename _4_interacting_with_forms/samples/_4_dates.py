import pytest
from common import config, helpers
from time import sleep
from selenium.webdriver import Remote


@pytest.fixture(scope="module")
def chrome():
    driver = helpers.new_driver("chrome")
    driver.get(config.WEB_SAMPLES_URL + "/sample5.html")
    yield driver
    driver.close()


@pytest.fixture(scope="module")
def firefox():
    driver = helpers.new_driver()
    driver.get(config.WEB_SAMPLES_URL + "/sample5.html")
    yield driver
    driver.close()


def test_set_native_date_in_chrome(chrome: Remote):
    # https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/date
    new_value = "2021-01-31"
    native_date = chrome.find_element_by_id("native-date-picker")
    # chrome.execute_script(f"arguments[0].value='{new_value}'", native_date)
    chrome.execute_script(f"document.querySelector('#native-date-picker').value='{new_value}'")

    # Print values of both fields to console
    assert native_date.get_property("value") == new_value

    # Sleep for a moment to show the dialog
    sleep(1)
    native_date.screenshot(config.SCREENSHOTS_DIR + "/native-date-picker-ch.png")


def test_set_native_date_in_firefox(firefox: Remote):
    # https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/date
    new_value = "2021-01-31"
    native_date = firefox.find_element_by_id("native-date-picker")
    firefox.execute_script(f"arguments[0].value='{new_value}'", native_date)

    # Print values of both fields to console
    assert native_date.get_property("value") == new_value

    # Sleep for a moment to show the dialog
    sleep(1)
    native_date.screenshot(config.SCREENSHOTS_DIR + "/native-date-picker-ff.png")
