from time import sleep
import pytest
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.webdriver import Remote

from common import config, helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample2.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_click_links(driver: Remote):
    # Find all links
    links = driver.find_elements_by_css_selector("#anchors a")
    # Click all the links
    for i, link in enumerate(links):
        if link.is_enabled():
            print(f"Click #{i}: '{link.text}'")
            link.click()
            sleep(1)

    new_links_texts = [link.text for link in links]

    assert "Here we go!" in new_links_texts
    assert "Link 1 (clicked: true)" in new_links_texts
    assert "Link 2 (clicked: true)" in new_links_texts
    assert "Link 3 (clicked: true)" in new_links_texts


def test_click_buttons(driver: Remote):
    # Find all not disabled buttons
    buttons = driver.find_elements_by_css_selector("#buttons button:not([disabled])")

    assert len(buttons) == 2

    # Click all the buttons
    for button in buttons:
        if button.is_enabled():
            button.click()
            sleep(1)


def test_click_disabled_button(driver: Remote):
    button = driver.find_element_by_css_selector("#buttons button[disabled]")

    try:
        button.click()
    except ElementClickInterceptedException:
        print("Element not clickable!")
