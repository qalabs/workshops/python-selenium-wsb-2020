import pytest
from time import sleep
from common import config, helpers
from selenium.webdriver import Remote


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample4.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_1(driver: Remote):
    # Get all selects list

    selects = driver.find_elements_by_css_selector("div.md-select")

    # Handle single selection

    select_single = selects[0]
    select_single.click()

    menu = driver.find_element_by_class_name("md-menu-content-container")
    menu_items = menu.find_elements_by_tag_name("li")
    menu_items[1].click()

    sleep(1)

    # Multiple selection

    select_multi = selects[1]
    select_multi.click()

    menu_items = driver.find_elements_by_class_name("md-list-item")
    menu_items[0].click()
    menu_items[1].click()

    # Sleep for a moment to show the dialog
    sleep(1)
