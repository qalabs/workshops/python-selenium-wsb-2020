import pytest
from selenium.webdriver import Remote
from selenium.webdriver import ActionChains

from common import helpers


@pytest.fixture
def driver():
    driver = helpers.new_driver()
    driver.get("https://jspaint.app")
    yield driver
    driver.close()


def test_paint(driver: Remote):
    canvas = driver.find_element_by_class_name("main-canvas")
    ac = ActionChains(driver)
    ac.click_and_hold(canvas)
    ac.move_by_offset(100, 0)
    ac.move_by_offset(0, 100)
    ac.move_by_offset(-100, 0)
    ac.move_by_offset(0, -100)
    ac.release()
    ac.perform()
