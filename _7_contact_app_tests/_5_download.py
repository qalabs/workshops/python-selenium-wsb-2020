import csv
import os

import pytest
from selenium.webdriver import Remote
from selenium.webdriver.support.wait import WebDriverWait

from common import config


@pytest.fixture
def driver(login):
    _remove_downloaded_csv_files()
    # Returns already logged in user
    return login


def test_download_all_contacts(driver: Remote):
    driver.find_element_by_css_selector("#contacts-list thead .v-input--selection-controls__ripple").click()
    driver.find_element_by_xpath("//*[contains(text(), 'Download')]").click()

    filepath = config.TMP_DIR + os.sep + "contacts.csv"

    wait = WebDriverWait(driver, 5)
    wait.until(lambda _: os.path.isfile(filepath), message="Expected file not found: " + filepath)

    csv_records = _read_contacts_from_csv()
    assert len(csv_records) == 3

    assert csv_records[0]["Name"] == "Fryderyk Maciejewski"
    assert csv_records[0]["Email"] == "fryderyk.maciejewski@teleworm.us"

    assert csv_records[1]["Name"] == "Julita Walczak"
    assert csv_records[1]["Email"] == "julita.walczak@rhyta.com"

    assert csv_records[2]["Name"] == "Emeryk Kaczmarek"
    assert csv_records[2]["Email"] == "emeryk.kaczmarek@rhyta.com"


def test_download_selected_contacts(driver: Remote):
    checkboxes = driver.find_elements_by_css_selector("tbody tr .v-input--selection-controls__ripple")
    checkboxes[0].click()
    checkboxes[1].click()

    driver.find_element_by_xpath("//*[contains(text(), 'Download')]").click()

    # wait for the file to be at the location
    filepath = config.TMP_DIR + os.sep + "contacts.csv"

    wait = WebDriverWait(driver, 5)
    wait.until(lambda _: os.path.isfile(filepath))

    csv_records = _read_contacts_from_csv()

    assert len(csv_records) == 2

    assert csv_records[0]["Name"] == "Fryderyk Maciejewski"
    assert csv_records[0]["Email"] == "fryderyk.maciejewski@teleworm.us"

    assert csv_records[1]["Name"] == "Emeryk Kaczmarek"
    assert csv_records[1]["Email"] == "emeryk.kaczmarek@rhyta.com"


def _remove_downloaded_csv_files():
    files = [file for file in os.listdir(config.TMP_DIR) if file.endswith(".csv")]
    for file in files:
        os.remove(config.TMP_DIR + os.sep + file)


def _read_contacts_from_csv():
    with open(file=f"{config.TMP_DIR}{os.sep}contacts.csv", mode="r") as csv_file:
        csv_reader = csv.DictReader(csv_file)
        data = []
        for row in csv_reader:
            data.append(row)
    return data
