import pytest
from selenium.webdriver import Remote
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


@pytest.fixture
def driver(login):
    return login


def test_get_contact_details(driver: Remote):
    search = driver.find_element_by_id("search-filter")
    search.send_keys("Emeryk Kaczmarek")

    driver.find_element_by_css_selector("tbody tr:nth-child(1) td:nth-child(3)").click()

    wait = WebDriverWait(driver, 5)
    contact_details: WebElement = wait.until(expected_conditions.visibility_of_element_located((By.ID, "contact-details")))

    assert "Emeryk Kaczmarek" == contact_details.find_element_by_css_selector("[data-property=contact-name]").text
