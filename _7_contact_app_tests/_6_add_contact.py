import os

import pytest
from selenium.webdriver import Remote
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


@pytest.fixture
def driver(login):
    return login


def test_add_contact_with_name(driver: Remote):
    driver.find_element_by_xpath("//button//*[contains(text(), 'New Contact')]").click()
    name = driver.find_element_by_css_selector("input[name=contact-name]")
    name.send_keys("Tomasz Nowak")

    driver.find_element_by_css_selector("button[type=submit]").click()

    wait = WebDriverWait(driver, 5)
    wait.until(expected_conditions.text_to_be_present_in_element((By.CSS_SELECTOR, "#snackbar .v-snack__content"), "Contact created successfully."))

    search = driver.find_element_by_id("search-filter")
    search.send_keys("Tomasz Nowak")

    cells = driver.find_elements_by_css_selector("#contacts-list tbody tr td:nth-child(3)")
    names = [cell.text for cell in cells]

    assert len(names) == 1
    assert "Tomasz Nowak" in names


def test_add_contact_with_avatar(driver: Remote):
    driver.find_element_by_xpath("//button//*[contains(text(), 'New Contact')]").click()
    name = driver.find_element_by_css_selector("input[name=contact-name]")
    name.send_keys("Tomasz Nowak")

    file = driver.find_element_by_css_selector("input[type=file]")
    file.send_keys(os.path.abspath("avatar.png"))

    wait = WebDriverWait(driver, 5)
    wait.until(lambda _: "data:image/png;base64" in driver.find_element_by_css_selector("#contact-form .v-image__image.v-image__image--cover").get_attribute("style"))

    driver.find_element_by_css_selector("button[type=submit]").click()

    search = driver.find_element_by_id("search-filter")
    search.send_keys("Tomasz Nowak")

    assert "data:image/png;base64" in driver.find_element_by_css_selector("tbody tr td .v-image__image.v-image__image--cover").get_attribute("style")

    cells = driver.find_elements_by_css_selector("#contacts-list tbody tr td:nth-child(3)")
    names = [cell.text for cell in cells]

    assert len(names) == 1
    assert "Tomasz Nowak" in names