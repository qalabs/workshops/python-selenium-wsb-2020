import pytest
from selenium.webdriver import ActionChains
from selenium.webdriver import Remote
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


@pytest.fixture
def driver(login):
    # Returns already logged in user
    return login


def test_get_default_contacts_names(driver: Remote):
    cells = driver.find_elements_by_css_selector("#contacts-list tbody tr td:nth-child(3)")
    names = [cell.text for cell in cells]

    assert len(names) == 3
    # assert "Emeryk Kaczmarek" in names
    # assert "Fryderyk Maciejewski" in names
    # assert "Julita Walczak" in names
    assert all(name in ["Emeryk Kaczmarek", "Fryderyk Maciejewski", "Julita Walczak"] for name in names)


def test_search_for_single_contact_by_name(driver: Remote):
    search = driver.find_element_by_id("search-filter")
    search.send_keys("emeryk")

    cells = driver.find_elements_by_css_selector("#contacts-list tbody tr td:nth-child(3)")
    names = [cell.text for cell in cells]

    assert len(names) == 1
    assert "Emeryk Kaczmarek" in names


def test_filter_by_co_workers_label_by_typing_into_the_field(driver: Remote):
    label = driver.find_element_by_id("label-filter")
    label.send_keys("Co-workers" + Keys.ENTER)

    cells = driver.find_elements_by_css_selector("#contacts-list tbody tr td:nth-child(3)")
    names = [cell.text for cell in cells]

    assert len(names) == 2
    assert all(name in ["Emeryk Kaczmarek", "Julita Walczak"] for name in names)


def test_filter_by_co_workers_label_by_selecting_an_option(driver: Remote):
    label_filter_input = driver.find_element_by_css_selector("#label-filter")
    label_filter_input.click()

    coworkers_label = driver.find_element_by_xpath("//*[@class='v-list__tile__title'][contains(text(), 'Co-workers')]")
    coworkers_label.click()

    cells = driver.find_elements_by_css_selector("#contacts-list tbody tr td:nth-child(3)")
    names = [cell.text for cell in cells]

    assert len(names) == 2
    assert all(name in ["Emeryk Kaczmarek", "Julita Walczak"] for name in names)


def test_delete_selected_contacts(driver: Remote):
    checkboxes = driver.find_elements_by_css_selector("#contacts-list tbody tr .v-input--selection-controls__ripple")
    checkboxes[0].click()
    checkboxes[1].click()

    delete = driver.find_element_by_xpath("//*[contains(text(), 'Delete')]")
    delete.click()

    alert = driver.switch_to.alert
    assert "Are you sure you want to delete selected contacts?" == alert.text
    alert.accept()

    wait = WebDriverWait(driver, 5)
    wait.until(expected_conditions.text_to_be_present_in_element((By.CSS_SELECTOR, "#snackbar .v-snack__content"), "Selected contacts removed successfully."))

    cells = driver.find_elements_by_css_selector("#contacts-list tbody tr td:nth-child(3)")
    names = [cell.text for cell in cells]

    assert len(names) == 1
    assert "Julita Walczak" in names


def test_show_favorites(driver: Remote):
    driver.find_element_by_css_selector(".v-toolbar .v-menu").click()
    driver.find_element_by_xpath("//*[contains(text(), 'Show favorites')]").click()

    wait = WebDriverWait(driver, 5)
    wait.until(expected_conditions.text_to_be_present_in_element((By.CSS_SELECTOR, "#snackbar .v-snack__content"), "Showing favorite contacts."))

    cells = driver.find_elements_by_css_selector("#contacts-list tbody tr td:nth-child(3)")
    names = [cell.text for cell in cells]

    assert len(names) == 1
    assert "Fryderyk Maciejewski" in names


#
# Optional
#

def test_search_for_two_contacts_by_email(driver: Remote):
    search_input = driver.find_element_by_id("search-filter")
    search_input.clear()
    search_input.send_keys("@rhyta.com")

    names = [td.text for td in driver.find_elements_by_css_selector("#contacts-list tbody tr td:nth-child(3)")]

    assert len(names) == 2
    assert all(n in ["Emeryk Kaczmarek", "Julita Walczak"] for n in names)


def test_search_for_no_results(driver: Remote):
    search_input = driver.find_element_by_id("search-filter")
    search_input.clear()
    search_input.send_keys("no_results")

    no_results = [td.text for td in driver.find_elements_by_css_selector("#contacts-list tbody tr td")]

    assert len(no_results) == 1
    assert no_results[0] == "No results for current filters."


def test_filter_by_two_labels(driver: Remote):
    label_filter_input = driver.find_element_by_css_selector("#label-filter")
    label_filter_input.click()

    coworkers_label = driver.find_element_by_xpath("//*[@class='v-list__tile__title'][contains(text(), 'Co-workers')]")
    coworkers_label.click()

    friends_label = driver.find_element_by_xpath("//*[@class='v-list__tile__title'][contains(text(), 'Friends')]")
    friends_label.click()

    names = [td.text for td in driver.find_elements_by_css_selector("#contacts-list tbody tr td:nth-child(3)")]

    assert len(names) == 3
    assert all(n in ["Emeryk Kaczmarek", "Fryderyk Maciejewski", "Julita Walczak"] for n in names)


def test_filter_by_non_existing_label(driver: Remote):
    label_filter_input = driver.find_element_by_css_selector("#label-filter")

    actions = ActionChains(driver)
    actions.click(label_filter_input)
    actions.send_keys("Non-existing")
    actions.send_keys(Keys.ENTER)
    actions.perform()

    no_results = [td.text for td in driver.find_elements_by_css_selector("#contacts-list tbody tr td")]

    assert len(no_results) == 1
    assert no_results[0] == "No data."
