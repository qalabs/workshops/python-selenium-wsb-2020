import pytest
from selenium.common.exceptions import WebDriverException

from common import helpers
from common import config
from selenium.webdriver import Remote


@pytest.fixture(scope="module")
def driver_instance():
    """
    Configure the driver for the module, close once all tests are executed

    :return: driver instance
    """
    driver = helpers.new_driver(driver="firefox", implicitly_wait=0.25)
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance: Remote):
    """
    Open Contacts App before each test and clear all cookies after each test

    :return: driver instance
    """
    driver_instance.get(config.CONTACTS_APP_URL)
    yield driver_instance
    driver_instance.delete_all_cookies()


@pytest.fixture
def login(driver: Remote):
    """
    Get initialized driver for this test and login to the application, returning the driver instance. Logout silently after the test.

    :param driver:
    :return:
    """
    try:
        driver.find_element_by_xpath("//nav//button/*[contains(text(), 'Log in')]").click()
        driver.find_element_by_css_selector("#username").send_keys("contacts")
        driver.find_element_by_css_selector("#password").send_keys("demo")
        driver.find_element_by_xpath("//*[@id='login-form-actions']/button/*[contains(text(), 'Log in')]").click()
        if not str(driver.current_url).endswith("/#/"):
            raise AssertionError("Login to the application (probably) failed.")
    except Exception as e:
        pytest.xfail("Could not setup the test. Details: " + str(e))
    yield driver
    try:
        logout = driver.find_element_by_xpath("//nav//i[contains(@class, 'v-icon')][contains(text(), 'logout')]")
        logout.click()
    except WebDriverException:
        print("Couldn't logout, ignoring. Were you logged in?")
