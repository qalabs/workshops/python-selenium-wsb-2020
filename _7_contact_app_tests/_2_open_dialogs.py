from selenium.webdriver import Remote
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


def test_open_login_dialog_and_close(driver: Remote):
    login = driver.find_element_by_xpath("//div[@class='v-dialog__container']//*[contains(text(), 'Log in')]")
    login.click()

    assert driver.find_element_by_css_selector(".v-dialog.v-dialog--active .v-card__title").text == "Log in"

    cancel = driver.find_element_by_xpath("//div[@id='login-form-actions']//*[contains(text(), 'Cancel')]/parent::button")
    cancel.click()

    assert len(driver.find_elements_by_css_selector(".v-dialog.v-dialog--active")) == 0


def test_open_login_dialog_and_close_with_web_driver_wait(driver: Remote):
    wait = WebDriverWait(driver, 5)

    login = driver.find_element_by_xpath("//div[@class='v-dialog__container']//*[contains(text(), 'Log in')]")
    login.click()

    dialog_is_opened = wait.until(expected_conditions.text_to_be_present_in_element((By.CSS_SELECTOR, ".v-dialog.v-dialog--active .v-card__title"), "Log in"))
    assert dialog_is_opened

    cancel = driver.find_element_by_xpath("//div[@id='login-form-actions']//*[contains(text(), 'Cancel')]/parent::button")
    cancel.click()

    dialog_is_closed = wait.until(expected_conditions.invisibility_of_element_located((By.CSS_SELECTOR, ".v-dialog.v-dialog--active")))
    assert dialog_is_closed


def test_open_signup_dialog_and_close(driver: Remote):
    signup = driver.find_element_by_xpath("//div[@class='v-dialog__container']//*[contains(text(), 'Signup')]")
    signup.click()

    assert driver.find_element_by_css_selector(".v-dialog.v-dialog--active .v-card__title").text == "Signup"

    cancel = driver.find_element_by_xpath("//div[@id='signup-form-actions']//*[contains(text(), 'Cancel')]/parent::button")
    cancel.click()

    assert len(driver.find_elements_by_css_selector(".v-dialog.v-dialog--active")) == 0


def test_open_signup_dialog_and_close_with_web_driver_wait(driver: Remote):
    wait = WebDriverWait(driver, 5)

    signup = driver.find_element_by_xpath("//div[@class='v-dialog__container']//*[contains(text(), 'Signup')]")
    signup.click()

    dialog_is_opened = wait.until(expected_conditions.text_to_be_present_in_element((By.CSS_SELECTOR, ".v-dialog.v-dialog--active .v-card__title"), "Signup"))
    assert dialog_is_opened

    cancel = driver.find_element_by_xpath("//div[@id='signup-form-actions']//*[contains(text(), 'Cancel')]/parent::button")
    cancel.click()

    dialog_is_closed = wait.until(expected_conditions.invisibility_of_element_located((By.CSS_SELECTOR, ".v-dialog.v-dialog--active")))
    assert dialog_is_closed


def test_open_about_dialog_and_close(driver: Remote):
    about = driver.find_element_by_xpath("//nav//i[contains(@class, 'v-icon')][contains(text(), 'help')]")
    about.click()

    assert driver.find_element_by_css_selector(".v-dialog.v-dialog--active .v-card__title").text == "About"

    close = driver.find_element_by_xpath("//*[contains(text(), 'Close')]")
    close.click()

    assert len(driver.find_elements_by_css_selector(".v-dialog.v-dialog--active")) == 0


def test_open_about_dialog_and_close_with_web_driver_wait(driver: Remote):
    wait = WebDriverWait(driver, 5)

    about = driver.find_element_by_xpath("//nav//i[contains(@class, 'v-icon')][contains(text(), 'help')]")
    about.click()

    dialog_is_opened = wait.until(expected_conditions.text_to_be_present_in_element((By.CSS_SELECTOR, ".v-dialog.v-dialog--active .v-card__title"), "About"))
    assert dialog_is_opened

    close = driver.find_element_by_xpath("//*[contains(text(), 'Close')]")
    close.click()

    dialog_is_closed = wait.until(expected_conditions.invisibility_of_element_located((By.CSS_SELECTOR, ".v-dialog.v-dialog--active")))
    assert dialog_is_closed
