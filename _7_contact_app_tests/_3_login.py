import pytest
from selenium.webdriver import Remote
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


def test_successful_login_and_logout(driver: Remote):
    driver.find_element_by_xpath("//nav//button/*[contains(text(), 'Log in')]").click()
    driver.find_element_by_css_selector("#username").send_keys("contacts")
    driver.find_element_by_css_selector("#password").send_keys("demo")
    driver.find_element_by_xpath("//*[@id='login-form-actions']/button/*[contains(text(), 'Log in')]").click()

    assert driver.current_url.endswith("/#/")

    logout = driver.find_element_by_xpath("//nav//i[contains(@class, 'v-icon')][contains(text(), 'logout')]")
    logout.click()

    assert driver.title == "Contacts App"
    assert driver.current_url.endswith("#/home")


def test_login_error_is_shown_on_invalid_credentials(driver: Remote):
    driver.find_element_by_xpath("//nav//button/*[contains(text(), 'Log in')]").click()
    driver.find_element_by_css_selector("#username").send_keys("invalid")
    driver.find_element_by_css_selector("#password").send_keys("credentials")
    driver.find_element_by_xpath("//*[@id='login-form-actions']/button/*[contains(text(), 'Log in')]").click()

    wait = WebDriverWait(driver, 5)
    wait.until(expected_conditions.visibility_of_element_located((By.ID, "snackbar")))

    assert "Invalid credentials" in driver.find_element_by_id("snackbar").text


def test_validation_error_is_shown_on_empty_or_too_short_username(driver: Remote):
    driver.find_element_by_xpath("//nav//button/*[contains(text(), 'Log in')]").click()

    driver.find_element_by_css_selector("#username").clear()
    driver.find_element_by_xpath("//*[@id='login-form-actions']/button/*[contains(text(), 'Log in')]").click()

    errors = [el.text for el in driver.find_elements_by_css_selector(".v-input .v-messages__message")]
    assert "Username is required." in errors

    driver.find_element_by_css_selector("#username").send_keys("a")
    driver.find_element_by_xpath("//*[@id='login-form-actions']/button/*[contains(text(), 'Log in')]").click()

    errors = [el.text for el in driver.find_elements_by_css_selector(".v-input .v-messages__message")]
    assert "Username must be at least 3 characters long" in errors


def test_validation_error_is_shown_on_empty_or_too_short_password(driver: Remote):
    driver.find_element_by_xpath("//nav//button/*[contains(text(), 'Log in')]").click()

    driver.find_element_by_css_selector("#password").clear()
    driver.find_element_by_xpath("//*[@id='login-form-actions']/button/*[contains(text(), 'Log in')]").click()

    errors = [el.text for el in driver.find_elements_by_css_selector(".v-input .v-messages__message")]
    assert "Password is required." in errors

    driver.find_element_by_css_selector("#password").send_keys("a")
    driver.find_element_by_xpath("//*[@id='login-form-actions']/button/*[contains(text(), 'Log in')]").click()

    errors = [el.text for el in driver.find_elements_by_css_selector(".v-input .v-messages__message")]
    assert "Password must be at least 3 characters long" in errors
