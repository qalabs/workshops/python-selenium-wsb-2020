import pytest
from selenium.webdriver import Remote


def test_is_at_home_page(driver: Remote):
    assert driver.title == "Contacts App"
    assert driver.current_url.endswith("#/home")
    assert "Manage your contacts with ease!" in driver.find_element_by_tag_name("h1").text
    assert "Contacts App" in driver.find_element_by_css_selector("h1+p").text


def test_navigate_to_login_page_and_back(driver: Remote):
    login = driver.find_element_by_xpath("//nav//*[contains(text(), 'Log in')]")
    login.click()

    assert driver.current_url.endswith("#/login")
    assert driver.find_element_by_css_selector(".v-toolbar .v-toolbar__title").text == "Log in"

    cancel = driver.find_element_by_xpath("//div[@id='login-form-actions']//*[contains(text(), 'Cancel')]/parent::button")
    cancel.click()

    assert driver.current_url.endswith("#/home")


def test_navigate_to_signup_page_and_back(driver: Remote):
    signup = driver.find_element_by_xpath("//nav//*[contains(text(), 'Signup')]")
    signup.click()

    assert driver.current_url.endswith("#/register")
    assert driver.find_element_by_css_selector(".v-toolbar .v-toolbar__title").text == "Signup"
    assert driver.find_element_by_css_selector("#signup .v-card__text").text == "Not implemented yet."

    cancel = driver.find_element_by_xpath("//div[@id='signup-form-actions']//*[contains(text(), 'Cancel')]/parent::button")
    cancel.click()

    assert driver.current_url.endswith("#/home")


def test_navigate_to_contact_us_page_and_back(driver: Remote):
    contact_us = driver.find_element_by_xpath("//nav//*[contains(text(), 'Contact Us')]")
    contact_us.click()

    assert driver.current_url.endswith("#/contact")
    assert driver.find_element_by_css_selector(".v-toolbar .v-toolbar__title").text == "Contact Us"
    assert driver.find_element_by_css_selector("#contact-us .v-card__text").text == "Not implemented yet."

    cancel = driver.find_element_by_xpath("//div[@class='v-card__actions']//*[contains(text(), 'Cancel')]/parent::button")
    cancel.click()

    assert driver.current_url.endswith("#/home")
